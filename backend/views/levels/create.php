<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Levels */

$this->title = Yii::t('app', 'Toifa qo\'shish');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Levels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="levels-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
