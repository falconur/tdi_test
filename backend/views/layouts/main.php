<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;

$bundle = yiister\gentelella\assets\Asset::register($this);

?>
<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">

    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="/" class="site_title"><span>TDI</span></a>
                </div>
                <div class="clearfix"></div>

                <!-- menu prile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <img src="/login/images/uzbekistan_gerb_31059.jpg" alt="..." class="img-circle profile_img">
                    </div>

                    <div class="profile_info">
                        <span>Admin</span>
                        <h2><?= Yii::$app->user->identity->username ?></h2>
                    </div>
                </div>
                <!-- /menu prile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3><?= Yii::t('app', 'Menu') ?></h3>
                        <?=
                        \yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => [
                                    ["label" => Yii::t('app', "Asosiy"), "url" => ["admin/dashboard"], "icon" => "home"],
                                    [
                                        "label" => Yii::t('app', "Testlar"),
                                        "icon" => "th",
                                        "url" => "#",
                                        "items" => [
                                            ["label" => Yii::t('app', "Hammasi"), "url" => ["questions/index"]],
                                            ["label" => Yii::t('app', "Qabul qilindi"), "url" => ["questions/accepted"]],
                                            ["label" => Yii::t('app', "Qabul qilinmadi"), "url" => ["questions/declined"]],
                                            ["label" => Yii::t('app', "Tekshirilmagan"), "url" => ["questions/waiting"]]
                                        ],
                                    ],
                                    ["label" => Yii::t('app', "Ekspertlar"), "url" => ["admin/experts"], "icon" => ""],
                                    ["label" => Yii::t('app', "Tuzuvchilar"), "url" => ["admin/teachers"], "icon" => ""],
                                    [
                                        "label" => Yii::t('app', "Tizimni boshqarish"),
                                        "url" => "#",
                                        "icon" => "table",
                                        "items" => [
                                            [
                                                "label" => Yii::t('app', "Testlarni ekspertizaga jo'natish"),
                                                "url" => ["admin/list"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Testlarni bazaga yozish"),
                                                "url" => "#",
                                            ],
                                        ],
                                    ],
                                    [
                                        "label" => Yii::t('app', "Hisobotlar"),
                                        "url" => "#",
                                        "icon" => "table",
                                        "items" => [
                                            [
                                                "label" => Yii::t('app', "Viloyatlar bo'yicha"),
                                                "url" => "#",
                                            ],
                                            [
                                                "label" => Yii::t('app', "Ekspertlar bo'yicha"),
                                                "url" => "#",
                                            ],
                                            [
                                                "label" => Yii::t('app', "Sifati"),
                                                "url" => "#",
                                            ],
                                        ],
                                    ],
                                    [
                                        "label" => Yii::t('app', "Ma'lumotlar"),
                                        "url" => "#",
                                        "icon" => "table",
                                        "items" => [
                                            [
                                                "label" => Yii::t('app', "Viloyatlar"),
                                                "url" => ["regions/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Tumanlar"),
                                                "url" => ["districts/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Tashkilotlar"),
                                                "url" => ["organisations/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Fanlar"),
                                                "url" => ["subjects/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Fan sohasi"),
                                                "url" => ["subject-types/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Toifalar"),
                                                "url" => ["levels/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Tillar"),
                                                "url" => ["languages/index"],
                                            ],
                                            [
                                                "label" => Yii::t('app', "Savol darajalari"),
                                                "url" => ["degrees/index"],
                                            ],
                                        ],
                                    ],
                                ],
                            ]
                        )
                        ?>
                    </div>

                </div>
                <!-- /sidebar menu -->                
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">

            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?= strtoupper(Yii::$app->language) ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?= Url::to(['', 'language' => 'uz']) ?>">UZ</a>
                                </li>
                                <li>
                                    <a href="<?= Url::to(['', 'language' => 'ru']) ?>">RU</a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="false">
                                <img src="/login/images/uzbekistan_gerb_31059.jpg" alt=""><?= Yii::$app->user->identity->username ?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="#<?php //echo Url::to(['teachers/update']) ?>"><?= Yii::t('app', 'Profile')?></a>
                                </li>
                                <li>
                                    <a href="javascript:;"><?= Yii::t('app', 'Yordam')?></a>
                                </li>
                                <li><a href="<?= Url::to(['site/logout']) ?>" data-method="post"><i
                                                class="fa fa-sign-out pull-right"></i><?= Yii::t('app', 'Chiqish') ?></a>
                                </li>
                            </ul>
                        </li>

                        <!-- <li role="presentation" class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>
                                <span class="badge bg-green">6</span>
                            </a>
                            <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                <li>
                                    <a>
                                        <span class="image">
                                            <img src="http://placehold.it/128x128" alt="Profile Image"/>
                                        </span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <span class="image">
                                            <img src="http://placehold.it/128x128" alt="Profile Image"/>
                                        </span>
                                        <span>
                                            <span>John Smith</span>
                                            <span class="time">3 mins ago</span>
                                        </span>
                                        <span class="message">
                                            Film festivals used to be do-or-die moments for movie makers. They were where...
                                        </span>
                                    </a>
                                </li>
                                <li>
                                    <div class="text-center">
                                        <a href="/">
                                            <strong>See All Alerts</strong>
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>

            <?= $content ?>
        </div>
        <!-- /page content -->        
    </div>

</div>

<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
<?php $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/latest.js?config=AM_CHTML'); ?>
</body>
</html>
<?php $this->endPage(); ?>
