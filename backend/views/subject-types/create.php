<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SubjectTypes */

$this->title = Yii::t('app', 'Soha qo\'shish');
//$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Subject Types'), 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subject-types-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
