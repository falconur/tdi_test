<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/23/18
 * Time: 5:51 AM
 */

use sjaakp\gcharts\PieChart;
?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Jami testlar") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["accepted"] + $numbers["declined"] + $numbers["waiting"];?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Tekshirilmagan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["waiting"] ?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Ekspertizadan o'tgan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["accepted"] ?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Ekspertizadan o'tmagan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["declined"] ?></div>
        </div>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <?php if (Yii::$app->user->identity->role === 3): ?>
            <strong><?= Yii::t('app', "Testlar holati bo'yicha") ?></strong>
        <?php else: ?>
            <strong><?= Yii::t('app', "Ekspertiza testlari holati") ?></strong>
        <?php endif; ?>
    </div>
    <div class="panel-body">
        <?= PieChart::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'status',
                    'type' => 'string',
                    'value' => function ($model) {
                        return $model->getStatus();
                    }
                ],
                'cnt'
            ]
        ]) ?>
    </div>
</div>

