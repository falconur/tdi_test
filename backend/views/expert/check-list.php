<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/25/18
 * Time: 2:37 AM
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\grid\GridView;
use common\models\Degrees;
use common\models\Subjects;
use common\models\Languages;

?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'text',
            [
                'attribute' => 'degree_id',
                'filter' => ArrayHelper::map(Degrees::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'degree.name_'. Yii::$app->language
            ],
            [
                'attribute' => 'subject_id',
                'filter' => ArrayHelper::map(Subjects::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'subject.name_'. Yii::$app->language
            ],
            'source',
            [
                'attribute' => 'lang_id',
                'filter' => ArrayHelper::map(Languages::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'lang.name_'. Yii::$app->language
            ],
            [
                'attribute' => 'status',
                'filter' => ['0' => 'Tekshirilmagan', '1' => 'Qabul qilindi', '-1' => 'Qabul qilinmadi'],
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            //'expert_id',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['expert/check', 'id' => $model->id]), [
                            'title' => Yii::t('app', 'Tekshirish'),
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>