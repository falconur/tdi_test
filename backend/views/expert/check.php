<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/25/18
 * Time: 2:44 AM
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Degrees;

?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => '3'])->label('Savol') ?>
    <p id="ques"></p>

    <?php foreach ($answers as $i => $answer) {
        echo $form->field($answer, "[$i]text")->textInput(['maxlength' => true])->label($i + 1 . '-javob');
        echo "<p id='ans". $i ."'></p>";
    }  ?>

    <?= $form->field($model, 'degree_id')->dropDownList(ArrayHelper::map(Degrees::find()->all(), 'id', 'name_uz')) ?>

    <?= $form->field($model, 'status')->dropDownList(['0' => 'Tekshirilmagan', '1' => 'Qabul qilindi', '-1' => 'Qabul qilinmadi']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>