<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            //'teacher_id',
            [
                'label' => '1-javob',
                'value' => function ($model) {
                    return $model->answers[0]->text;

                }
            ],
            [
                'label' => '2-javob',
                'value' => function ($model) {
                    return $model->answers[1]->text;

                }
            ],
            [
                'label' => '3-javob',
                'value' => function ($model) {
                    return $model->answers[2]->text;

                }
            ],
            [
                'label' => '4-javob',
                'value' => function ($model) {
                    return $model->answers[3]->text;

                }
            ],
            'degree.name_'. Yii::$app->language,
            'subject.name_' . Yii::$app->language,
            'source',
            'lang.name_'. Yii::$app->language,
            [
                'attribute' => 'status',
                'value' => function ($model) {
                        return $model->getStatus();
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->formatter->asDate($model->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->formatter->asDate($model->updated_at)
            ],
        ],
    ]) ?>

</div>
