<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\Degrees;
use common\models\Subjects;
use common\models\Languages;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Savollar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'text',
            [
                'attribute' => 'degree_id',
                'filter' => ArrayHelper::map(Degrees::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'degree.name_'. Yii::$app->language
            ],
            [
                'attribute' => 'subject_id',
                'filter' => ArrayHelper::map(Subjects::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'subject.name_'. Yii::$app->language
            ],
            'source',
            [
                'attribute' => 'lang_id',
                'filter' => ArrayHelper::map(Languages::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'lang.name_'. Yii::$app->language
            ],
            [
                'attribute' => 'status',
                'filter' => ['0' => Yii::t('app', 'Tekshirilmagan'), '1' => Yii::t('app', 'Qabul qilindi'), '-1' => Yii::t('app', 'Qabul qilinmadi')],
                'value' => function ($model) {
                    return $model->getStatus();
                }
            ],
            //'expert_id',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
