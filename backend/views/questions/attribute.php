<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/15/18
 * Time: 11:58 AM
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Languages;
use common\models\Subjects;

?>

<?php $this->registerCss('.margin-fix { margin-top: 10px; }'); ?>

<?= Html::beginForm(Url::to(['questions/attribute'])) ?>

<?= Html::label(Yii::t('app', "Til")) ?>

<?= Html::dropDownList('lang', null, ArrayHelper::map(Languages::find()->all(), 'id', 'name_'. Yii::$app->language), ['placeholder' => Yii::t('app', "Til tanlang"), 'class' => 'form-control']) ?>

<?= Html::label(Yii::t('app', "Fan"), null, ['class' => 'margin-fix']) ?>

<?= Html::dropDownList('subj', null, ArrayHelper::map(Subjects::find()->all(), 'id', 'name_'. Yii::$app->language), ['placeholder' => Yii::t('app', "Fan tanlang"), 'class' => 'form-control']) ?>

<?= Html::submitButton(Yii::t('app', 'Tasdiqlash'), ['class' => 'btn btn-success margin-fix']) ?>

<?= Html::endForm(); ?>



