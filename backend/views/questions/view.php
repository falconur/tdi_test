<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Taxrirlash'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'O\'chirish'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text',
            //'teacher_id',
            [
                'label' => '1'. Yii::t('app', '-javob'),
                'value' => $model->answers[0]->text
            ],
            [
                'label' => '2'. Yii::t('app', '-javob'),
                'value' => $model->answers[1]->text
            ],
            [
                'label' => '3'. Yii::t('app', '-javob'),
                'value' => $model->answers[2]->text
            ],
            [
                'label' => '4'. Yii::t('app', '-javob'),
                'value' => $model->answers[3]->text                
            ],
            [
                'attribute' => 'degree_id',
                'value' => (Yii::$app->language == 'ru') ? $model->degree->name_ru : $model->degree->name_uz
            ],
            [
                'attribute' => 'subject_id',
                'value' => (Yii::$app->language == 'ru') ? $model->subject->name_ru : $model->subject->name_uz
            ],
            [
                'attribute' => 'lang_id',
                'value' => (Yii::$app->language == 'ru') ? $model->lang->name_ru : $model->lang->name_uz
            ],            
            'source',
            [
                'attribute' => 'status',
                'value' => $model->getStatus()                
            ],
            [
                'attribute' => 'teacher_id',
                'value' => $model->teacher->teachers->first_name
            ],

            [
                'attribute' => 'created_at',
                'value' => Yii::$app->formatter->asDate($model->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->formatter->asDate($model->updated_at)
            ],
        ],
    ]) ?>

</div>
