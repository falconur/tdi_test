<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Questions */

$this->title = Yii::t('app', 'Yangi savol');
?>
<div class="questions-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'answers' => $answers
    ]) ?>

</div>
