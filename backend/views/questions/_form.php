<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\Degrees;

/* @var $this yii\web\View */
/* @var $model common\models\Questions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => '3'])->label(Yii::t('app', 'Savol')) ?>
    <p id="ques"></p>

    <?php foreach ($answers as $i => $answer) {
        echo $form->field($answer, "[$i]text")->textInput(['maxlength' => true])->label($i + 1 . Yii::t('app', '-javob'));
        echo "<p id='ans". $i ."'></p>";
    }  ?>

    <?= $form->field($model, 'degree_id')->dropDownList(ArrayHelper::map(Degrees::find()->all(), 'id', 'name_'. Yii::$app->language)) ?>

    <?= $form->field($model, 'source')->textarea(['rows' => '3']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Saqlash'), ['class' => 'btn btn-success']) ?>

        <?= Html::a(Yii::t('app', 'Yakunlash'), Url::to(['expert/waiting']), ['class' => 'btn btn-info']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>