<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use common\models\Districts;
use common\models\Levels;
use common\models\SubjectTypes;
use common\models\Organisations;
use common\models\Languages;

/* @var $this yii\web\View */
/* @var $model common\models\Teachers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teachers-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <div class="raw">
        <div class="col-sm-4">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-6">
            <?= $form->field($model, 'birthday')
                ->widget(DatePicker::className(), [
                //'language' => 'ru',
                //'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control']
            ]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-6">
            <?= $form->field($model, 'district_id')->dropDownList(
                    ArrayHelper::map(Districts::find()->all(), 'id', 'name_'. Yii::$app->language),
                    ['prompt' => Yii::t('app', "Tuman/shahar tanlang")])
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-6">
            <?= $form->field($model, 'level_id')->dropDownList(
                    ArrayHelper::map(Levels::find()->all(), 'id', 'name_'. Yii::$app->language),
                    ['prompt' => Yii::t('app', "Toifangizni tanlang")])
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'level_date')
                ->widget(DatePicker::className(), [
                //'language' => 'ru',
                //'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control']
            ]) ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-6">
            <?= $form->field($model, 'experience')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'type_id')->dropDownList(
                    ArrayHelper::map(SubjectTypes::find()->all(), 'id', 'name_'. Yii::$app->language),
                    ['prompt' => Yii::t('app', "Fan yo'nalishini tanlang")])
            ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-6">
            <?= $form->field($model, 'organ_id')->dropDownList(
                    ArrayHelper::map(Organisations::find()->all(), 'id', 'name_'. Yii::$app->language),
                    ['prompt' => Yii::t('app', "Tashkilotingizni tanlang")])
            ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'lang_id')->dropDownList(
                    ArrayHelper::map(Languages::find()->all(), 'id', 'name_'. Yii::$app->language),
                    ['prompt' => Yii::t('app', "Tilni tanlang")]);
            ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-4">
            <?= $form->field($model, 'f_inn')->fileInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'f_inps')->fileInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'f_cv')->fileInput() ?>
        </div>
    </div>

    <div class="raw">
        <div class="col-sm-4">
            <?= $form->field($model, 'f_passport')->fileInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'f_diploma')->fileInput() ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'f_avatar')->fileInput() ?>
        </div>
    </div>

    <div class="form-group col-sm-4">
        <?= Html::submitButton(Yii::t('app', 'Saqlash'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
