<?php

use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\models\User;
use common\models\Subjects;

$this->title = Yii::t('app', 'Savollar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject')->dropDownList(ArrayHelper::map(Subjects::find()->all(), 'id', 'name_'. Yii::$app->language )) ?>

    <?= $form->field($model, 'user')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(User::find()->andWhere('role = 2')->all(), 'id', 'teachers.first_name'),
            'options' => ['placeholder' => Yii::t('app', 'Tanlang yoki kiriting')],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <?= $form->field($model, 'number')->textInput(['type' => 'number', 'placeholder' => Yii::t('app', 'Test sonini kiriting')]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Yuborish'), ['class' => 'btn btn-success']); ?>
    </div>

    <?php $form = ActiveForm::end(); ?>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>            
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>            
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('notice')): ?>
        <div class="alert alert-info alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>            
            <?= Yii::$app->session->getFlash('notice') ?>
        </div>
    <?php endif; ?>
    
    <?= Html::endForm() ?>
</div>