<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/27/18
 * Time: 9:35 AM
 */

use sjaakp\gcharts\PieChart;
use yii\grid\GridView;
use sjaakp\gcharts\BarChart;
?>

<div class="row">
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-info">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Jami testlar") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["accepted"] + $numbers["declined"] + $numbers["waiting"];?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-warning">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Tekshirilmagan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["waiting"] ?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Ekspertizadan o'tgan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["accepted"] ?></div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <strong><?= Yii::t('app', "Ekspertizadan o'tmagan") ?></strong>
            </div>
            <div class="panel-body"><?= $numbers["declined"] ?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?php if (Yii::$app->user->identity->role === 3): ?>
                    <strong><?= Yii::t('app', "Testlar holati bo'yicha") ?></strong>
                <?php else: ?>
                    <strong><?= Yii::t('app', "Ekspertiza testlari holati") ?></strong>
                <?php endif; ?>
            </div>
            <div class="panel-body">
                <?= PieChart::widget([
                    'dataProvider' => $dataQuestions,
                    'columns' => [
                        [
                            'attribute' => 'status',
                            'type' => 'string',
                            'value' => function ($model) {
                                return $model->getStatus();
                            }
                        ],
                        'cnt'
                    ]
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><?= Yii::t('app', 'Fanlar miqyosida savollar ulushi')?></strong>
            </div>
            <div class="panel-body">
                <?= BarChart::widget([
                    'dataProvider' => $questionsSubject,
                    'columns' => [
                        [
                            'attribute' => 'subject_id',
                            'type' => 'string',
                            'value' => function ($model) {
                                if (Yii::$app->language == 'uz') {
                                    return $model->subject->name_uz;
                                } else {
                                    return $model->subject->name_ru;
                                }                                
                            }
                        ],
                        'cnt'
                    ]
                ]) ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><?= Yii::t('app', 'Eng yaxshi ekspertlar')?></strong>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataExperts,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'label' => Yii::t('app', 'FIO'),
                            'value' => function($model) {
                                return $model->last_name. " " .$model->first_name. " " .$model->middle_name;
                            }
                        ],

                        'organ.name_'. Yii::$app->language,
                        'district.name_'. Yii::$app->language,
                        //'email:email',

                        //'status',
                        //'created_at',
                        //'updated_at',
                    ]
                ]); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong><?= Yii::t('app', 'Eng yaxshi tuzuvchilar')?></strong>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $dataCreators,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'label' => Yii::t('app', 'FIO'),
                            'value' => function($model) {
                                return $model->last_name. " " .$model->first_name. " " .$model->middle_name;
                            }
                        ],

                        'organ.name_'. Yii::$app->language,
                        'district.name_'. Yii::$app->language,
                        //'email:email',

                        //'status',
                        //'created_at',
                        //'updated_at',
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
