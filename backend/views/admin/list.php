<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/29/18
 * Time: 12:12 PM
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\Degrees;
use common\models\Subjects;
use common\models\Languages;
use yii\grid\GridView;
use kartik\select2\Select2;
use common\models\Teachers;
use common\models\User;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\QuestionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Savollar');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    --><?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
      //  'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'subject_id',
                'filter' => ArrayHelper::map(Subjects::find()->all(), 'id', 'name_'. Yii::$app->language),
                'value' => 'subject.name_'. Yii::$app->language
            ],
            [
                'attribute' => 'cnt',
                'label' => Yii::t('app', 'Testlar soni')
            ],            
            
            //'expert_id',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{assign}',
                'buttons' => [
                    'assign' => function ($url, $model, $key) {
                        return Html::a('', Url::to(['admin/assign', 'id' => $model->subject_id]),
                            [
                                'class' => 'fa fa-send',
                                'title' => Yii::t('app', 'Ekspertga jo\'natish'),
                                'data-method' => 'post',
                                'data-params' => ['id' => 'id', 'num' => 'num']
                            ]
                        );                        
                    }

                ]
            ],
        ],
    ]); ?>

    <?= Html::endForm() ?>
</div>
