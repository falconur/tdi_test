<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/19/18
 * Time: 7:59 AM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?> -->
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'attribute' => 'fullname',  
                'label' => Yii::t('app', 'FIO'),
                'value' => function($model) {
                    return $model->last_name. " " .$model->first_name. " " .$model->middle_name;
                }
                
            ],

            [
                'label' => Yii::t('app', 'Tashkilot'),
                'value' => 'organ.name_' . Yii::$app->language,
            ],

            [
                'label' => Yii::t('app', 'Tuman'),
                'value' => 'district.name_' . Yii::$app->language,
            ],

            //'email:email',
            [
                'label' => Yii::t('app', 'Vazifa'),
                'value' => function($model) {
                    return $model->user->getRole();
                }
            ],
            [
                'attribute' => 'status_teacher',
                'format' => 'Html',
                'value' => function ($model) {
                    if ($model->status_teacher === 0)
                        return Html::a(null, ['teachers/status', 'id' => $model->id],
                            [
                                'class' => 'fa fa-minus-square-o',
                                'style' => 'font-size: 24px; color:red',
                                'title' => Yii::t('app', 'Faol emas'),
                            ]);
                    else
                        return Html::a(null, ['teachers/status', 'id' => $model->id],
                            [
                                'class' => 'fa fa-check-square-o',
                                'style' => 'font-size: 24px; color:green',
                                'title' => Yii::t('app', 'Faol'),
                            ]);
                },
                'filter' => ['0' => Yii::t('app', 'Faol emas'), '1' => Yii::t('app', 'Faol')]
            ],            
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
