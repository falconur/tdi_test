<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-t-85 p-b-20">
            <span class="login100-form-title p-b-50">
                O'ZBEKISTON RESPUBLIKASI VAZIRLAR MAHKAMASI HUZURIDAGI TA'LIM SIFATINI NAZORAT QILISH DAVLAT INSPEKSIYASI
                </span>
                <span class="login100-form-avatar">
                    <img src="/login/images/uzbekistan_gerb_31059.jpg" alt="AVATAR">
                </span>
                <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                <div class="wrap-input100 m-t-85 m-b-35">
                    <?= $form->field($model, 'username', [
                        'template' => '{label}{input}<span class="focus-input100" data-placeholder="Username"></span>{error}'
                    ])->textInput(['class' => 'input100', 'autofocus' => true,])->label(false) ?>
                </div>

                <div class="wrap-input100 m-b-50">
                    <?= $form->field($model, 'email', [
                        'template' => '{label}{input}<span class="focus-input100" data-placeholder="Email"></span>{error}'
                    ])->textInput(['class' => 'input100'])->label(false) ?>
                </div>

                <div class="wrap-input100 m-b-50">
                    <?= $form->field($model, 'password', [
                        'template' => '{label}{input}<span class="focus-input100" data-placeholder="Password"></span>{error}'
                    ])->passwordInput(['class' => 'input100'])->label(false) ?>
                </div>

                <div class="container-login100-form-btn">
                    <?= Html::submitButton('Sign up', ['class' => 'login100-form-btn', 'name' => 'login-button']) ?>
                </div>
                <?php ActiveForm::end(); ?>

                <ul class="login-more p-t-190">
                    <li>
                        <span class="txt1">Have you an account?</span>
                        <?= Html::a("Log in", Url::to(['site/login']), ['class' => 'txt2']) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
