<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Teachers;

/**
 * TeachersSearch represents the model behind the search form of `common\models\Teachers`.
 */
class TeachersSearch extends Teachers
{
    public $fullname;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'birthday', 'district_id', 'level_id', 'level_date', 'type_id', 'organ_id', 'lang_id', 'status_teacher', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'middle_name', 'phone', 'address', 'experience', 'cv', 'passport', 'diploma', 'inn', 'inps', 'avatar', 'fullname'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Teachers::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['fullname'] = [
            'asc' => ['last_name' => SORT_ASC, 'first_name' => SORT_ASC, 'middle_name' => SORT_ASC],
            'desc' => ['last_name' => SORT_DESC, 'first_name' => SORT_DESC, 'middle_name' => SORT_DESC],
            'label' => 'FullName',
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'birthday' => $this->birthday,
            'district_id' => $this->district_id,
            'level_id' => $this->level_id,
            'level_date' => $this->level_date,
            'type_id' => $this->type_id,
            'organ_id' => $this->organ_id,
            'lang_id' => $this->lang_id,
            'status_teacher' => $this->status_teacher,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->fullname])
            ->andFilterWhere(['like', 'last_name', $this->fullname])
            ->andFilterWhere(['like', 'middle_name', $this->fullname])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'experience', $this->experience])
            ->andFilterWhere(['like', 'cv', $this->cv])
            ->andFilterWhere(['like', 'passport', $this->passport])
            ->andFilterWhere(['like', 'diploma', $this->diploma])
            ->andFilterWhere(['like', 'inn', $this->inn])
            ->andFilterWhere(['like', 'inps', $this->inps])
            ->andFilterWhere(['like', 'avatar', $this->avatar]);

        return $dataProvider;
    }
}
