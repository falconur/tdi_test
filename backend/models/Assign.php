<?php

namespace backend\models;

use Yii;
use yii\base\Model;

class Assign extends Model
{   
    public $user, $subject, $number;

    public function rules()
    {
        return [
            [['user', 'subject', 'number'], 'integer']
        ];
    }    

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user' => Yii::t('app', 'Ekspert'),
            'subject' => Yii::t('app', 'Fan'),
            'number' => Yii::t('app', 'Testlar soni'),            
        ];
    }
}
