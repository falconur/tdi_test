<?php

namespace backend\controllers;

use common\models\Teachers;
use Yii;
use backend\models\UserSearch;
use common\models\Questions;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use common\models\User;
use backend\models\TeachersSearch;
use yii\helpers\Url;
use backend\models\Assign;
use yii\base\Model;

class AdminController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Lists all Experts models.
     * @return mixed
     */
    public function actionExperts()
    {
        $searchModel = new TeachersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);                
        $dataProvider->query->joinWith('user');
        $dataProvider->query->andWhere('user.role = 2');

        return $this->render('experts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Teachers models.
     * @return mixed
     */
    public function actionTeachers()
    {
        $searchModel = new TeachersSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);                
        $dataProvider->query->joinWith('user');        
        $dataProvider->query->andWhere('user.role = 3');

        return $this->render('experts', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Declined Questions models.
     * @return mixed
     */
    public function actionList()
    {       
        $dataProvider = new ActiveDataProvider([
            'query' => Questions::find()
                ->select('COUNT(*) as cnt, subject_id')
                ->andWhere('status = 0')
                ->groupBy('subject_id'),
            'pagination' => false
        ]);
        
        return $this->render('list', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionAssign($id = null)
    {
        $model = new Assign();        
        
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->post()) {                
                
                $questions = Questions::find()
                    ->andWhere('subject_id ='. $model->subject)
                    ->andWhere('expert_id IS NULL')
                    ->limit($model->number)
                    ->all();
                // echo "<pre>";
                //     return print_r($questions);
                // echo "</pre>";
                $number = 0;
                foreach ($questions as $question) {
                    $question->expert_id = $model->user;
                    if ($question->save()) {
                        $number++;
                    }                                          
                }            
                
                if ($number == $model->number) {
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Testlar muvaffaqiyatli jo\'natildi'));
                } elseif ($number == 0) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Xatolik. Testlar jo\'natilmadi'));
                } elseif ($number < $model->number) {
                    Yii::$app->session->setFlash('notice', Yii::t('app', 'Testlar soni yetarli emas'));
                } 
            }    
        }
        
        $model->subject = $id;

        return $this->render('assign', [
            'model' => $model
        ]);
    }

    /**
     * @return string
     */
    public function actionDashboard()
    {
        $numbers = [
            "accepted" => count(Questions::find()
                ->andWhere('status ='. 1)
                ->all()),
            "declined" => count(Questions::find()
                ->andWhere('status ='. -1)
                ->all()),
            "waiting" => count(Questions::find()
                ->andWhere('status ='. 0)
                ->all()),
        ];        

        $dataQuestions = new ActiveDataProvider([
            'query' => Questions::find()
                ->select('COUNT(*) as cnt, status')
                ->groupBy('status'),
            'pagination' => false
        ]);

        $questionsSubject = new ActiveDataProvider([
            'query' => Questions::find()
                ->select('COUNT(*) as cnt, subject_id')
                //->andWhere('status = 1')
                ->groupBy('subject_id'),
            'pagination' => false
        ]);

        $dataExperts = new ActiveDataProvider([
            'query' => Teachers::find()
                ->innerJoin('user', 'user.id = teachers.user_id')
                ->andWhere('user.role = 2')
                ->limit(10)
        ]);

        $dataCreators = new ActiveDataProvider([
            'query' => Teachers::find()
                ->innerJoin('user', 'user.id = teachers.user_id')
                ->andWhere('user.role = 3')
                ->limit(10)
        ]);

        return $this->render('dashboard', [
            'numbers' => $numbers,
            'dataQuestions' => $dataQuestions,
            'dataExperts' => $dataExperts,
            'dataCreators' => $dataCreators,
            'questionsSubject' => $questionsSubject
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Questions::findOne($id);
        $answers = [];

        for ($i = 0; $i < 4; $i++) {
            $answers[$i] = $model->answers[$i];
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($answers, Yii::$app->request->post())) {
            $model->status = 1;
            $model->save();

            foreach ($answers as $answer) {                
                $answer->save();
            }

            return $this->redirect(['questions/view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'answers' => $answers
        ]);

        
    }

}
