<?php

namespace backend\controllers;

use Yii;
use common\models\Questions;
use backend\models\QuestionsSearch;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\Url;

class ExpertController extends BaseController
{
    public function actionIndex()
    {
        return $this->redirect(['accepted']);
    }

    /**
     * Lists all Accepted Questions models of user.
     * @return mixed
     */
    public function actionAccepted()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = 1');
        $dataProvider->query->andWhere('teacher_id = '. Yii::$app->user->id);

        return $this->render('questions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Declined Questions models of user.
     * @return mixed
     */
    public function actionDeclined()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = -1');
        $dataProvider->query->andWhere('teacher_id = '. Yii::$app->user->id);

        return $this->render('questions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Declined Questions models of user.
     * @return mixed
     */
    public function actionWaiting()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = 0');
        $dataProvider->query->andWhere('teacher_id = '. Yii::$app->user->id);

        return $this->render('questions', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all checked Questions models of expert.
     * @return mixed
     */
    public function actionChecked()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status != 0');
        $dataProvider->query->andWhere('expert_id = '. Yii::$app->user->id);
        Url::remember();

        return $this->render('check-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all unchecked Questions models of expert.
     * @return mixed
     */
    public function actionUnchecked()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = 0');
        $dataProvider->query->andWhere('expert_id = '. Yii::$app->user->id);
        Url::remember();

        return $this->render('check-list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCheck($id)
    {
        $model = $this->findModel($id);
        $answers = [];

        for ($i = 0; $i < 4; $i++) {
            $answers[] = $model->answers[$i];
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($answers, Yii::$app->request->post())) {
            $model->save(false);

            return $this->redirect(Url::previous());
        }

        return $this->render('check', [
            'model' => $model,
            'answers' => $answers
        ]);
    }

    /**
     * @return string
     */
    public function actionDashboard()
    {
        $numbers = [
            "accepted" => count(Questions::find()
                ->andWhere('teacher_id ='. Yii::$app->user->id)
                ->andWhere('status ='. 1)
                ->all()),
            "declined" => count(Questions::find()
                ->andWhere('teacher_id ='. Yii::$app->user->id)
                ->andWhere('status ='. -1)
                ->all()),
            "waiting" => count(Questions::find()
                ->andWhere('teacher_id ='. Yii::$app->user->id)
                ->andWhere('status ='. 0)
                ->all()),
        ];

        if (Yii::$app->user->identity->role === 3) {
            $dataProvider = new ActiveDataProvider([
                'query' => Questions::find()
                    ->select('COUNT(*) as cnt, status')
                    ->andWhere('teacher_id ='. Yii::$app->user->id)
                    ->groupBy('status'),
                'pagination' => false
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Questions::find()
                    ->select('COUNT(*) as cnt, status')
                    ->andWhere('expert_id ='. Yii::$app->user->id)
                    ->groupBy('status'),
                'pagination' => false
            ]);
        }

        return $this->render('dashboard', [
            "numbers" => $numbers,
            "dataProvider" => $dataProvider
        ]);
    }

    public function actionUpdate($id)
    {
        $this->redirect(['questions/update', 'id' => $id]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
