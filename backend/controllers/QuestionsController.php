<?php

namespace backend\controllers;

use common\models\Answers;
use Yii;
use common\models\Questions;
use backend\models\QuestionsSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * QuestionsController implements the CRUD actions for Questions model.
 */
class QuestionsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Questions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $session = Yii::$app->session;
        if ($session->isActive) {
            $session->destroy();
        }

        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Accepted Questions models.
     * @return mixed
     */
    public function actionAccepted()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = 1');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Declined Questions models.
     * @return mixed
     */
    public function actionDeclined()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = -1');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all Declined Questions models.
     * @return mixed
     */
    public function actionWaiting()
    {
        $searchModel = new QuestionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere('status = 0');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAttribute()
    {
        if (Yii::$app->request->post()) {
            $session = Yii::$app->session;

            $session->open();
            $session->set('lang', Yii::$app->request->post('lang'));
            $session->set('subj', Yii::$app->request->post('subj'));

            return $this->redirect(['create']);
        }

        return $this->render('attribute');
    }

    /**
     * Creates a new Questions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questions();
        $answers = [];

        for ($i = 0; $i < 4; $i++) {
            $answers[] = new Answers();
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($answers, Yii::$app->request->post())){
            $model->teacher_id = Yii::$app->user->id;
            $model->status = 0;

            $session = Yii::$app->session;
            $model->lang_id = $session->get('lang');
            $model->subject_id = $session->get('subj');

            $model->save();

            foreach ($answers as $i => $answer) {
                $answer->question_id = $model->id;

                if ($i == 0) {
                    $answer->true = 1;
                } else {
                    $answer->true = 0;
                }

                $answer->save();
            }

            return $this->redirect(['create']);
        }

        return $this->render('create', [
            'model' => $model,
            'answers' => $answers
        ]);
    }

    /**
     * Updates an existing Questions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (Yii::$app->user->identity->role == 1) {
            return $this->redirect(['admin/update', 'id' => $id]);        
        }
        $model = $this->findModel($id);
        $answers = [];

        for ($i = 0; $i < 4; $i++) {
            $answers[$i] = $model->answers[$i];
        }

        if ($model->load(Yii::$app->request->post()) && Model::loadMultiple($answers, Yii::$app->request->post())) {
            $model->status = 1;
            $model->save();

            foreach ($answers as $answer) {                
                $answer->save();
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'answers' => $answers
        ]);
    }

    /**
     * Deletes an existing Questions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Questions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
