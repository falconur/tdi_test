<?php

namespace backend\controllers;

use Yii;
use common\models\Teachers;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Url;

/**
 * TeachersController implements the CRUD actions for Teachers model.
 */
class TeachersController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Teachers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Teachers::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Teachers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Teachers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Teachers();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            $model->birthday = strtotime($model->birthday);
            $model->level_date = strtotime($model->level_date);
            $model->status_teacher = 0;

            $model->f_cv = UploadedFile::getInstance($model, 'f_cv');
            if (isset($model->f_cv)) {
                $filename = "upload/cv". $model->f_cv->baseName .'.'. $model->f_cv->extension;
                $model->f_cv->saveAs($filename, false);
                $model->cv = $filename;
            }            

            $model->f_inn = UploadedFile::getInstance($model, 'f_inn');
            if (isset($model->f_inn)) {
                $filename = "upload/inn". $model->f_inn->baseName .'.'. $model->f_inn->extension;
                $model->f_inn->saveAs($filename, false);
                $model->inn = $filename;
            }
            
            $model->f_inps = UploadedFile::getInstance($model, 'f_inps');
            if (isset($model->f_inps)) {
                $filename = "upload/inps". $model->f_inps->baseName .'.'. $model->f_inps->extension;
                $model->f_inps->saveAs($filename, false);
                $model->inps = $filename;
            }

            $model->f_passport = UploadedFile::getInstance($model, 'f_passport');
            if (isset($model->f_passport)) {
                $filename = "upload/passport". $model->f_passport->baseName .'.'. $model->f_passport->extension;
                $model->f_passport->saveAs($filename, false);
                $model->passport = $filename;
            }

            $model->f_diploma = UploadedFile::getInstance($model, 'f_diploma');
            if (isset($model->f_diploma)) {
                $filename = "upload/diploma". $model->f_diploma->baseName .'.'. $model->f_diploma->extension;
                $model->f_diploma->saveAs($filename, false);
                $model->diploma = $filename;
            }

            $model->f_avatar = UploadedFile::getInstance($model, 'f_avatar');
            if (isset($model->f_avatar)) {
                $filename = "upload/avatar". $model->f_avatar->baseName .'.'. $model->f_avatar->extension;
                $model->f_avatar->saveAs($filename, false);
                $model->avatar = $filename;
            }

            $model->save(false);

            return $this->redirect(['view', 'id' => $model->user_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionStatus($id)
    {
        $teacher = Teachers::findOne($id);
        if ($teacher->status_teacher === 0) {
            $teacher->status_teacher = 1;
        } else {
            $teacher->status_teacher = 0;
        }
        $teacher->save();
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Updates an existing Teachers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate()
    {
        if (($model = Teachers::findOne(['user_id' => Yii::$app->user->id])) === null) {
            return $this->redirect(['create']);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Teachers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Teachers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Teachers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Teachers::findOne(['user_id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
