<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/10/18
 * Time: 12:34 PM
 */

namespace backend\controllers;

use Yii;

class BaseController extends \yii\web\Controller
{
    public function beforeAction($action) {
        if (isset(Yii::$app->user->identity->role))
        {
            switch (Yii::$app->user->identity->role) {
                case 1:
                    $this->layout = "main";
                    break;
                case 2:
                    $this->layout = "expert";
                    break;
                case 3:
                    $this->layout = "test";
                    break;
            }
        }
        return parent::beforeAction($action);
    }
}