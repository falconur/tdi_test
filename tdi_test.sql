-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 03, 2019 at 12:38 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tdi_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `question_id` int(11) NOT NULL,
  `true` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `text`, `question_id`, `true`, `created_at`, `updated_at`) VALUES
(53, 'To’g’ri chiziqli harakat deb trayektoriyasi to’g’ri chiziqdan iborat bo’lgan harakatga aytiladi Egri chiziqli harakat deb, trayektoriyasi egri chiziqdan iborat bo’lgan harakatga aytiladi', 21, 1, 1543816975, 1543816975),
(54, 'dasdasdИdasdasd', 21, 0, 1543816975, 1544260957),
(55, 'C', 21, 0, 1543816975, 1543816975),
(56, 'D', 21, 0, 1543816975, 1543816975),
(57, 'O’rtacha tezlik deb, umumiy bosib o’tilgan masofani shu masofani bosib o’tish uchunketgan vaqtga nisbatiga aytiladi. Oniy tezlik deb, jism trayektoriyasining ma’lum bir nuqtasidagi tezligiga yoki harakatining ma’lum bir vaqtidagi tezligiga aytiladi.', 22, 1, 1543817003, 1543817003),
(58, 'B', 22, 0, 1543817003, 1543817003),
(59, 'C', 22, 0, 1543817003, 1543817003),
(60, 'D', 22, 0, 1543817003, 1543817003),
(61, 'Tekis tezlanuvchan harakat deb, jismning bir xil vaqt oralig’ida oniy tezligi bir xil ortib boradigan harakatga aytiladi. Tekis sekinlanuvchan harakat deb, jismning bir xil vaqt oralig’ida oniy tezligi bir xil kamayib boradigan harakatga aytiladi.', 23, 1, 1543817057, 1543817057),
(62, 'B', 23, 0, 1543817057, 1543817057),
(63, 'C', 23, 0, 1543817057, 1543817057),
(64, 'D', 23, 0, 1543817057, 1543817057),
(65, 'Tekis tezlanuvchan harakatda tezlik- `vartheta=underset(vartheta)(0)+at`  Tekis sekinlanuvchan harakatda tezlik- `vartheta=underset(vartheta)(0)+at`', 24, 1, 1543817643, 1543817643),
(66, 'B', 24, 0, 1543817643, 1543817643),
(67, 'C', 24, 0, 1543817643, 1543817643),
(68, 'D', 24, 0, 1543817643, 1543817643),
(69, 'Elastiklik kuchi deb, jismni deformatsiyalanayotganda paydo bo’lib, jism molekulalarining siljishiga qarama-qarshi yo’nalishda hosil bo’lgan kuchga aytiladi', 25, 1, 1543817681, 1543817681),
(70, 'B', 25, 0, 1543817681, 1543817681),
(71, 'C', 25, 0, 1543817681, 1543817681),
(72, 'D', 25, 0, 1543817681, 1543817681),
(73, 'Ishqalanish kuchi deb, jismlar bir-biri ustida sirpanishda hosil bo’lib, jismlarning harakatida qarshilik ko’rsatuvchi kuchga aytiladi.', 26, 1, 1543817711, 1543817711),
(74, 'B', 26, 0, 1543817711, 1543817711),
(75, 'C', 26, 0, 1543817711, 1543817711),
(76, 'D', 26, 0, 1543817711, 1543817711),
(77, 'Butun olam tortishish qonuni: Ikki jismlar orasidagi o’zaro ta’sirlashuv kuchi massalari ko’paytmasiga to’g’ri mutanosib, ularning og’irliklari markazlari orasidagi masofasiga esa teskari mutanosib bo’ladi.', 27, 1, 1543817739, 1543817739),
(78, 'B', 27, 0, 1543817739, 1543817739),
(79, 'C', 27, 0, 1543817739, 1543817739),
(80, 'D', 27, 0, 1543817739, 1543817739),
(81, 'Kosmik tezliklar deb, Kosmik kemaga biror bir maqsad asosida kosmosga chiqarish uchun berilgan boshlang\'ich tezlikka aytiladi.', 28, 1, 1543817765, 1543817765),
(82, 'B', 28, 0, 1543817765, 1543817765),
(83, 'C', 28, 0, 1543817765, 1543817765),
(84, 'D', 28, 0, 1543817765, 1543817765),
(85, 'Jism impulsi deb, jism massasi bilan uning olgan tezligining ko’paytmasiga aytiladi. Kuch impulsi deb, jism impulsining o’zgarishiga aytiladi.', 29, 1, 1543817788, 1543817788),
(86, 'B', 29, 0, 1543817788, 1543817788),
(87, 'C', 29, 0, 1543817788, 1543817788),
(88, 'D', 29, 0, 1543817788, 1543817788),
(89, 'Impulsning saqlanish qonuni: yopiq sistemada to’qnashuvchi jismlarning to’qnashishdan oldingi impulslar vector yig’indisi ularning to’qnashishdan keyingi impulslarning vector yig’indisiga teng bo’ladi.', 30, 1, 1543817809, 1543817809),
(90, 'B', 30, 0, 1543817809, 1543817809),
(91, 'C', 30, 0, 1543817809, 1543817809),
(92, 'D', 30, 0, 1543817809, 1543817809),
(93, 'P1 (0;1) nuqtaning alfa burchakka burish natichasida hosil bo`lgan nuqtaning ordinatasiga shu burchakning sinusi deyiladi', 31, 1, 1543817935, 1543817935),
(94, 'B', 31, 0, 1543817935, 1543817935),
(95, 'C', 31, 0, 1543817935, 1543817935),
(96, 'D', 31, 0, 1543817935, 1543817935),
(97, '57', 32, 1, 1543817952, 1543817952),
(98, 'B', 32, 0, 1543817952, 1543817952),
(99, 'C', 32, 0, 1543817952, 1543817952),
(100, 'D', 32, 0, 1543817952, 1543817952),
(101, 'P1 (0;1) nuqtaning alfa burchakka burish natichasida hosil bo`lgan nuqtaning ordinatasiga shu burchakning sinusi deyiladi', 33, 1, 1543817970, 1543817970),
(102, 'B', 33, 0, 1543817970, 1543817970),
(103, 'C', 33, 0, 1543817970, 1543817970),
(104, 'D', 33, 0, 1543817970, 1543817970),
(105, 'B(t) nuqta irdinatasining shu nuqta abssissasiga nisbati (agar bu nisbat mavjud bo‘lsa) t sinning tangensi dayiladi', 34, 1, 1543818053, 1543818053),
(106, 'B', 34, 0, 1543818053, 1543818053),
(107, 'C', 34, 0, 1543818053, 1543818053),
(108, 'D', 34, 0, 1543818053, 1543818053),
(109, 'Alfa burchakning cosinusi deb (1;0) nuqtani kordinata boshi atrofida alfa burchakka burish tatijasidan hosil bo`lgan burchakak aytiladi', 35, 1, 1543818071, 1543818071),
(110, 'B', 35, 0, 1543818071, 1543818071),
(111, 'C', 35, 0, 1543818071, 1543818071),
(112, 'D', 35, 0, 1543818071, 1543818071),
(113, 'α  sonning  kotangensi  deb,   shu son  kosinusining   uning  sinusiga  nisbatiga  aytiladi', 36, 1, 1543818123, 1543818123),
(114, 'B', 36, 0, 1543818123, 1543818123),
(115, 'C', 36, 0, 1543818123, 1543818123),
(116, 'D', 36, 0, 1543818123, 1543818123),
(117, '6 ta', 37, 1, 1543818176, 1543818176),
(118, 'B', 37, 0, 1543818176, 1543818176),
(119, 'C', 37, 0, 1543818176, 1543818176),
(120, 'D', 37, 0, 1543818176, 1543818176),
(121, '1 va 2', 38, 1, 1543818196, 1543818196),
(122, 'b', 38, 0, 1543818196, 1543818196),
(123, 'c', 38, 0, 1543818196, 1543818196),
(124, 'd', 38, 0, 1543818196, 1543818196),
(125, '3 va 4', 39, 1, 1543818217, 1543818217),
(126, 'B', 39, 0, 1543818217, 1543818217),
(127, 'c', 39, 0, 1543818217, 1543818217),
(128, 'd', 39, 0, 1543818217, 1543818217),
(129, '2 va 3', 40, 1, 1543818236, 1543818236),
(130, 'b', 40, 0, 1543818236, 1543818236),
(131, 'c', 40, 0, 1543818236, 1543818236),
(132, 'd', 40, 0, 1543818236, 1543818236),
(133, 'So’zlarning grammatik ma’nolarini o’rganuvchi tilshunoslik bo’limi.', 41, 1, 1543818499, 1543818499),
(134, 'b', 41, 0, 1543818499, 1543818499),
(135, 'c', 41, 0, 1543818499, 1543818499),
(136, 'd', 41, 0, 1543818499, 1543818499),
(137, 'Egalik va kelishik qo’shimchalarini olib turlanish xususiyatiga ega bo’lgan so’zlar.', 42, 1, 1543818516, 1543818516),
(138, 'b', 42, 0, 1543818516, 1543818516),
(139, 'c', 42, 0, 1543818516, 1543818516),
(140, 'd', 42, 0, 1543818516, 1543818516),
(141, 'Ismlar guruhiga ot, olmosh, sifat, son, harakat nomi, sifatdosh, taqlidlar kiradi.', 43, 1, 1543818554, 1543818554),
(142, 'b', 43, 0, 1543818554, 1543818554),
(143, 'c', 43, 0, 1543818554, 1543818554),
(144, 'd', 43, 0, 1543818554, 1543818554),
(145, '–gach, -(i)b, -a(y), -guncha, -gancha, -gani.', 44, 1, 1543818584, 1543818584),
(146, 'b', 44, 0, 1543818584, 1543818584),
(147, 'c', 44, 0, 1543818584, 1543818584),
(148, 'd', 44, 0, 1543818584, 1543818584),
(149, 'Ish-harakat jarayonida bajaruvchining qay darajada ishtirok etishiga  fe’l nisbatlari deb aytiladi.', 45, 1, 1543818597, 1543818597),
(150, 'b', 45, 0, 1543818597, 1543818597),
(151, 'c', 45, 0, 1543818597, 1543818597),
(152, 'd', 45, 0, 1543818597, 1543818597),
(153, '5 ta', 46, 1, 1543818620, 1543818620),
(154, 'b', 46, 0, 1543818620, 1543818620),
(155, 'c', 46, 0, 1543818620, 1543818620),
(156, 'd', 46, 0, 1543818620, 1543818620),
(157, 'Sanoq sonlar bilan birga qo’llanib miqdor bildiruvchi so’zlar.', 47, 1, 1543818637, 1543818637),
(158, 'b', 47, 0, 1543818637, 1543818637),
(159, 'c', 47, 0, 1543818637, 1543818637),
(160, 'd', 47, 0, 1543818637, 1543818637),
(161, 'Grammatik, leksik, affiksal, frazeaologik omonimiya.', 48, 1, 1543818653, 1543818653),
(162, 'b', 48, 0, 1543818653, 1543818653),
(163, 'c', 48, 0, 1543818653, 1543818653),
(164, 'd', 48, 0, 1543818653, 1543818653),
(165, '3 tomonlama.', 49, 1, 1543818672, 1543818672),
(166, 'b', 49, 0, 1543818672, 1543818672),
(167, 'c', 49, 0, 1543818672, 1543818672),
(168, 'd', 49, 0, 1543818672, 1543818672),
(169, '4 ta.', 50, 1, 1543822187, 1543822187),
(170, 'b', 50, 0, 1543822187, 1543822187),
(171, 'c', 50, 0, 1543822187, 1543822187),
(172, 'd', 50, 0, 1543822187, 1543822187),
(173, 'a', 51, 1, 1543822649, 1543822649),
(174, 's', 51, 0, 1543822649, 1543822649),
(175, 'd', 51, 0, 1543822649, 1543822649),
(176, 't', 51, 0, 1543822649, 1543822649);

-- --------------------------------------------------------

--
-- Table structure for table `degrees`
--

CREATE TABLE `degrees` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `degrees`
--

INSERT INTO `degrees` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'Yuqori', 'Высокий', 123, 123),
(2, 'O\'rta', 'Cредний', 123, 123),
(3, 'Past', 'Низкий', 123, 123);

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `region_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name_uz`, `name_ru`, `region_id`, `created_at`, `updated_at`) VALUES
(1, 'Andijon', 'Андижан', 1, 123, 123),
(2, 'Asaka', 'Асака', 1, 1543360353, 1543360353);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'o`zbek', 'узбек', 123, 123),
(2, 'rus', 'рус', 123, 123),
(3, 'qirg\'iz', 'киргизский', 1543372765, 1543372765);

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'toifa', '', 123, 132),
(2, 'katta', 'старший', 1543372210, 1543372210);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1541846132),
('m130524_201442_init', 1541846133),
('m181108_195644_regions', 1541846133),
('m181108_195656_districts', 1541846133),
('m181108_195812_languages', 1541846133),
('m181108_195935_levels', 1541846133),
('m181108_200154_subject_types', 1541846133),
('m181108_200210_subjects', 1541846133),
('m181108_200248_organisations', 1541846133),
('m181108_200339_teachers', 1541846134),
('m181108_200406_questions', 1541846210),
('m181108_200414_answers', 1541846210);

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE `organisations` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'Xalq Ta\'limi', '', 123, 132),
(2, 'Oliy ta\'lim vazirligi', '', 123, 123),
(3, 'Sog\'liqni saqlash', 'Здравохранение', 1543361000, 1543361000);

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `degree_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `source` varchar(255) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `expert_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `text`, `teacher_id`, `degree_id`, `subject_id`, `source`, `lang_id`, `status`, `expert_id`, `created_at`, `updated_at`) VALUES
(21, 'To’g’ri chiziqli va egri chiziqli harakatlar deb qanday harakatga aytiladi?dasd\r\n', 4, 3, 2, '', 1, 1, 4, 1543816975, 1544260957),
(22, 'O’rtacha va oniy tezlik deb nimaga aytiladi?', 4, 2, 2, '', 1, 0, 4, 1543817003, 1544257049),
(23, 'Tekis tezlanuvchan va tekis sekinlanuvchan harakatlar deb qanday harakatga aytiladi?', 4, 1, 2, '', 1, 0, 4, 1543817057, 1544254102),
(24, 'Tekis tezlanuvchan va tekis sekinlanuvchan harakatda tezlik formulasi qanday bo’ladi?', 4, 1, 2, '', 1, 1, 4, 1543817643, 1544254196),
(25, 'Elastiklik kuchi qanday ta’riflanadi?', 4, 1, 2, '', 1, 0, 4, 1543817681, 1544256909),
(26, 'Ishqalanish kuchi qanday ta’riflanadi?', 4, 2, 2, '', 1, 0, 4, 1543817711, 1544254213),
(27, 'Butun olam tortishish qonuni qanday ta’riflanadi?', 4, 2, 2, '', 1, 0, 4, 1543817739, 1544254213),
(28, 'Kosmik tezliklar deb nimaga aytiladi?', 4, 2, 2, '', 1, 0, 4, 1543817765, 1544254213),
(29, 'Jism impulsi va Kuch impulsi deb nimaga aytiladi?', 4, 2, 2, '', 1, 0, 4, 1543817788, 1544256983),
(30, 'Impulsning saqlanish qonuni qanday ta’riflanadi?', 4, 1, 2, '', 1, 0, 4, 1543817809, 1544254213),
(31, 'Birlik aylana deb nimaga aytiladi ? ', 4, 3, 1, '\r\n', 1, 0, 4, 1543817935, 1544254090),
(32, 'Bir radian necha gradius?', 4, 3, 1, '', 1, 0, 4, 1543817952, 1544254090),
(33, 'Burchakning sinusi deb nimaga aytiladi?', 4, 3, 1, '', 1, 0, 4, 1543817970, 1544254090),
(34, 'Burchakning tangensi deb nimaga aytiladi?', 4, 3, 1, '', 1, 0, 4, 1543818053, 1544254227),
(35, 'Burchakning kosinusi deb nimaga aytiladi?', 4, 3, 1, '', 1, 0, 4, 1543818071, 1544254227),
(36, 'Burchakning kotangensi deb nimaga aytiladi? ', 4, 3, 1, '', 1, 0, 4, 1543818123, 1544254227),
(37, 'Asosiy trigonometrik ayniyatlar nechta ?', 4, 3, 1, '', 1, 0, 4, 1543818176, 1544254227),
(38, 'Sinus qaysi chorakda musbat?', 4, 3, 1, '', 1, 0, 4, 1543818196, 1544254227),
(39, 'Sinus qaysi chorakda manfiy?', 4, 2, 1, '', 1, 0, 4, 1543818217, 1544254934),
(40, 'kosinusi qaysi chorakda manfiy?', 4, 3, 1, '', 1, 0, 4, 1543818236, 1544254949),
(41, 'Morfologiya deb nimaga aytiladi?', 4, 2, 3, '', 2, 1, 4, 1543818499, 1544267746),
(42, 'Ismlar deb nimaga aytiladi?', 4, 3, 3, '\r\n', 1, 0, 4, 1543818516, 1544254245),
(43, 'Ismlar guruhiga qanday so’zlar kiradi?', 4, 2, 3, '', 1, 0, 4, 1543818554, 1544254245),
(44, 'Ravishdosh shaklining qo’shimchalari qaysilar?', 4, 1, 3, '\r\n', 1, 0, 4, 1543818584, 1544254245),
(45, 'Fe`l nisbatlari nima?', 4, 2, 3, '', 1, 0, 4, 1543818597, 1544254245),
(46, 'Quyidagi gapda nechta yordamchi so’zlar ishtirok etgan? \r\nXuddi so’z kabi gapning ham turi va xili, matndagi o’rniga qarab ma’nolari ko’p.', 4, 1, 3, '', 1, 0, NULL, 1543818620, 1543818620),
(47, 'Hisob so’zlar nima?', 4, 1, 3, '', 1, 0, NULL, 1543818637, 1543818637),
(48, 'Omonimlarning turlari qaysilar?', 4, 1, 3, '\r\n', 1, 0, NULL, 1543818653, 1543818653),
(49, 'Unlilar  necha tomonlama tasnif qilinadi? ', 4, 1, 3, '', 1, 0, NULL, 1543818672, 1543818672),
(50, 'Sifat darajalari nechta?', 4, 1, 1, '', 1, 0, 4, 1543822187, 1544256905),
(51, '`sqrt(akjkjhgkjhg)`', 4, 1, 1, '', 1, 1, NULL, 1543822649, 1544265841);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'Andijon', '', 123, 132),
(2, 'Buxoro', 'Бухара', 1543359564, 1543359564);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `type_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name_uz`, `name_ru`, `type_id`, `created_at`, `updated_at`) VALUES
(1, 'Matematika', 'Математика', 1, 123, 123),
(2, 'Fizika', 'Физика', 1, 1543371314, 1543371314),
(3, 'Ona tili', 'Родной язык (узбекский)', 2, 123, 123);

-- --------------------------------------------------------

--
-- Table structure for table `subject_types`
--

CREATE TABLE `subject_types` (
  `id` int(11) NOT NULL,
  `name_uz` varchar(255) NOT NULL,
  `name_ru` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject_types`
--

INSERT INTO `subject_types` (`id`, `name_uz`, `name_ru`, `created_at`, `updated_at`) VALUES
(1, 'Aniq fanlar', '', 123, 123),
(2, 'Ijtimoiy', 'Гуманитарный', 1543372095, 1543372095);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `birthday` int(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `level_id` int(11) DEFAULT NULL,
  `level_date` int(11) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `organ_id` int(11) DEFAULT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `cv` varchar(255) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `diploma` varchar(255) DEFAULT NULL,
  `inn` varchar(255) DEFAULT NULL,
  `inps` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `status_teacher` smallint(6) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `user_id`, `first_name`, `last_name`, `middle_name`, `birthday`, `phone`, `district_id`, `address`, `level_id`, `level_date`, `experience`, `type_id`, `organ_id`, `lang_id`, `cv`, `passport`, `diploma`, `inn`, `inps`, `avatar`, `status_teacher`, `created_at`, `updated_at`) VALUES
(7, 4, 'Nuriddin', 'Kamardinov', 'Saloxiddinovich', 1, '998905433722', 1, 'Bogishamol street 1-uy', 2, 1, 'test', 2, 3, 1, 'upload/cvMeeting-Home-1-1024x361.jpg', 'upload/passportinnovation-y-design.png', 'upload/diplomalogo-1040x585.jpg', 'upload/innGIST-Regional StartupTraining-SocialMedia-Uzbekistan-1.jpg', 'upload/inpse16465344c9c0b0bd751093b82fec44d_large.png', 'upload/avatarlogo-1040x585.jpg', 0, 1543816651, 1544187670),
(9, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1544168197, 1544187109);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'tMysqRiZUWlLJRxiqtAmWn3Ms2ODrIOf', '$2y$13$V7PWmrdwl0OMC2efSzpn8ehuCIFTPBvfPltQJ3Xjph0G3pEuDNhnK', NULL, 'user@mail.com', 1, 10, 1541724507, 1541724507),
(4, 'falconur', 'VsanBaatkLmUxZnEETgk4QniJIIzwf5z', '$2y$13$FfqBSQ2AA.C6P2u2OcH4UuyO6BAkduEG2yUlRonOE7nKQm96TbIn2', NULL, 'falconur98@gmail.com', 2, 10, 1543815537, 1543815537),
(8, 'tester', 'zR3w76Fp7crOLZhcGzWTKd1yDRYKNbPo', '$2y$13$GQyoDq8t/JHRGt2lJt7bvekpgYEUe5jr6aZfMexy2yAa1pSH//mXO', NULL, 'fds@fdaf.fd', 3, 10, 1544168197, 1544168197);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_answers_question` (`question_id`);

--
-- Indexes for table `degrees`
--
ALTER TABLE `degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_districts_region` (`region_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `organisations`
--
ALTER TABLE `organisations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_questions_subject` (`subject_id`),
  ADD KEY `fk_questions_lang` (`lang_id`),
  ADD KEY `fk_questions_teacher` (`teacher_id`),
  ADD KEY `fk_questions_degree` (`degree_id`),
  ADD KEY `fk_questions_expert` (`expert_id`);

--
-- Indexes for table `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subjects_type` (`type_id`);

--
-- Indexes for table `subject_types`
--
ALTER TABLE `subject_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_teacher_user` (`user_id`),
  ADD KEY `fk_teachers_district` (`district_id`),
  ADD KEY `fk_teachers_level` (`level_id`),
  ADD KEY `fk_teachers_type` (`type_id`),
  ADD KEY `fk_teachers_organ` (`organ_id`),
  ADD KEY `fk_teachers_lang` (`lang_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;
--
-- AUTO_INCREMENT for table `degrees`
--
ALTER TABLE `degrees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `organisations`
--
ALTER TABLE `organisations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `regions`
--
ALTER TABLE `regions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subject_types`
--
ALTER TABLE `subject_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `fk_answers_question` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `fk_districts_region` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `fk_questions_degree` FOREIGN KEY (`degree_id`) REFERENCES `degrees` (`id`),
  ADD CONSTRAINT `fk_questions_expert` FOREIGN KEY (`expert_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `fk_questions_lang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `fk_questions_subject` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_questions_teacher` FOREIGN KEY (`teacher_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `fk_subjects_type` FOREIGN KEY (`type_id`) REFERENCES `subject_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `teachers`
--
ALTER TABLE `teachers`
  ADD CONSTRAINT `fk_teacher_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_teachers_district` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`),
  ADD CONSTRAINT `fk_teachers_lang` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`),
  ADD CONSTRAINT `fk_teachers_level` FOREIGN KEY (`level_id`) REFERENCES `levels` (`id`),
  ADD CONSTRAINT `fk_teachers_organ` FOREIGN KEY (`organ_id`) REFERENCES `organisations` (`id`),
  ADD CONSTRAINT `fk_teachers_type` FOREIGN KEY (`type_id`) REFERENCES `subject_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;