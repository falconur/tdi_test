<?php

use yii\db\Migration;

/**
 * Class m181108_200414_answers
 */
class m181108_200414_answers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('answers', [
            'id' => $this->primaryKey(),
            'text' => $this->string()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'true' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_answers_question',
            'answers',
            'question_id',
            'questions',
            'id',
            'CASCADE',
            'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('answers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_200414_answers cannot be reverted.\n";

        return false;
    }
    */
}
