<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'role' => $this->smallInteger()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->insert('user', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => 'tMysqRiZUWlLJRxiqtAmWn3Ms2ODrIOf',
            'password_hash' => '$2y$13$V7PWmrdwl0OMC2efSzpn8ehuCIFTPBvfPltQJ3Xjph0G3pEuDNhnK',
            'email' => 'user@mail.com',
            'status' => 10,
            'role' => 1,
            'created_at' => 1541724507,
            'updated_at' => 1541724507
        ]);

        $this->insert('user', [
            'id' => 3,
            'username' => 'expert',
            'auth_key' => 'LxluAZe3KcDIzr5UaIRPh4K_RffUqpQW',
            'password_hash' => '$2y$13$7Ofp0lK1w1bzf2kMX0apeuKVh46EGinoKowX55N507lKuqHK.IGE6',
            'email' => 'user@test.com',
            'status' => 10,
            'role' => 2,
            'created_at' => 1541724519,
            'updated_at' => 1541724519
        ]);

        $this->insert('user', [
            'id' => 2,
            'username' => 'creator',
            'auth_key' => 'lJin_nDbOIM-c25DUmRu4wCjiDzP_Hnq',
            'password_hash' => '$2y$13$9bd5jxYay92Xxe2YkeLaie2SuytukTVHAnc1E6UIwVEZVHPQcpR2O',
            'email' => 'user1@test.com',
            'status' => 10,
            'role' => 3,
            'created_at' => 1541845960,
            'updated_at' => 1541845960
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
