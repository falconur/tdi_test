<?php

use yii\db\Migration;

/**
 * Class m181108_200406_questions
 */
class m181108_200406_questions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('questions', [
            'id' => $this->primaryKey(),
            'text' => $this->string()->notNull(),
            'teacher_id' => $this->integer()->notNull(),
            'degree' => $this->smallInteger()->notNull(),
            'subject_id' => $this->integer()->notNull(),
            'source' => $this->string()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'expert_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_questions_teacher',
            'questions',
            'teacher_id',
            'teachers',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_questions_expert',
            'questions',
            'expert_id',
            'teachers',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_questions_subject',
            'questions',
            'subject_id',
            'subjects',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->addForeignKey(
            'fk_questions_lang',
            'questions',
            'lang_id',
            'languages',
            'id',
            'RESTRICT',
            'RESTRICT');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('questions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_200406_questions cannot be reverted.\n";

        return false;
    }
    */
}
