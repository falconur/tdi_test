<?php

use yii\db\Migration;

/**
 * Class m181108_200339_teachers
 */
class m181108_200339_teachers extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('teachers', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'middle_name' => $this->string()->notNull(),
            'birthday' => $this->date(),
            'phone' => $this->string(),
            'district_id' => $this->integer()->notNull(),
            'address' => $this->string()->notNull(),
            'level_id' => $this->integer()->notNull(),
            'level_date' => $this->date()->notNull(),
            'experience' => $this->string()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'organ_id' => $this->integer()->notNull(),
            'lang_id' => $this->integer()->notNull(),
            'cv' => $this->string(),
            'passport' => $this->string()->notNull(),
            'diploma' => $this->string()->notNull(),
            'inn' => $this->string()->notNull(),
            'inps' => $this->string()->notNull(),
            'status' => $this->smallInteger()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull()
        ], $tableOptions);

        $this->addForeignKey(
            'fk_teacher_user',
            'teachers',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'RESTRICT');

        $this->addForeignKey(
            'fk_teachers_district',
            'teachers',
            'district_id',
            'districts',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_teachers_level',
            'teachers',
            'level_id',
            'levels',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_teachers_type',
            'teachers',
            'type_id',
            'subject_types',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_teachers_organ',
            'teachers',
            'organ_id',
            'organisations',
            'id',
            'RESTRICT',
            'RESTRICT');

        $this->addForeignKey(
            'fk_teachers_lang',
            'teachers',
            'lang_id',
            'languages',
            'id',
            'RESTRICT',
            'RESTRICT');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('teachers');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181108_200339_teachers cannot be reverted.\n";

        return false;
    }
    */
}
