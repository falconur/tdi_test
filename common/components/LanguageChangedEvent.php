<?php
/**
 * Created by PhpStorm.
 * User: falconur
 * Date: 11/24/18
 * Time: 1:50 AM
 */

namespace common\components;

use yii\base\Event;

class LanguageChangedEvent extends Event
{
    /**
     * @var string the new language
     */
    public $language;
    /**
     * @var string|null the old language
     */
    public $oldLanguage;

}