<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "questions".
 *
 * @property int $id
 * @property string $text
 * @property int $teacher_id
 * @property int $degree_id
 * @property int $subject_id
 * @property string $source
 * @property int $lang_id
 * @property int $status
 * @property int $expert_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Answers[] $answers
 * @property Degrees $degree
 * @property Teachers $expert
 * @property Languages $lang
 * @property Subjects $subject
 * @property User $teacher
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'questions';
    }

    public $cnt;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'teacher_id', 'degree_id', 'subject_id', 'lang_id', 'status'], 'required'],
            [['teacher_id', 'degree_id', 'subject_id', 'lang_id', 'status', 'expert_id', 'created_at', 'updated_at'], 'integer'],
            [['text', 'source'], 'string', 'max' => 255],
            [['degree_id'], 'exist', 'skipOnError' => true, 'targetClass' => Degrees::className(), 'targetAttribute' => ['degree_id' => 'id']],
            [['expert_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['expert_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['subject_id' => 'id']],
            [['teacher_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teacher_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Savol'),
            'teacher_id' => Yii::t('app', 'Tuzuvchi'),
            'degree_id' => Yii::t('app', 'Darajasi'),
            'subject_id' => Yii::t('app', 'Fan'),
            'source' => Yii::t('app', 'Manba'),
            'lang_id' => Yii::t('app', 'Til'),
            'status' => Yii::t('app', 'Holati'),
            'expert_id' => Yii::t('app', 'Tekshiruvchi'),
            'created_at' => Yii::t('app', 'Tuzilgan'),
            'updated_at' => Yii::t('app', 'O\'zgartirilgan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(Answers::className(), ['question_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegree()
    {
        return $this->hasOne(Degrees::className(), ['id' => 'degree_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpert()
    {
        return $this->hasOne(User::className(), ['id' => 'expert_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_id']);
    }

    public function getStatus()
    {
        switch ($this->status) {
            case -1:
                return Yii::t('app', "Ekspertizadan o'tmagan");
                break;
            case 0:
                return Yii::t('app', "Tekshirilmagan");
                break;
            case 1:
                return Yii::t('app', "Ekspertizadan o'tgan");
                break;
        }
    }
}
