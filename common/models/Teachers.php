<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "teachers".
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $birthday
 * @property string $phone
 * @property int $district_id
 * @property string $address
 * @property int $level_id
 * @property string $level_date
 * @property string $experience
 * @property int $type_id
 * @property int $organ_id
 * @property int $lang_id
 * @property string $cv
 * @property string $passport
 * @property string $diploma
 * @property string $inn
 * @property string $inps
 * @property string $avatar
 * @property int $status_teacher
 * @property int $created_at
 * @property int $updated_at 
 *
 * @property Questions[] $questions
 * @property User $user
 * @property Districts $district
 * @property Languages $lang
 * @property Levels $level
 * @property Organisations $organ
 * @property SubjectTypes $type
 */
class Teachers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teachers';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public $f_inn, $f_inps, $f_cv, $f_passport, $f_diploma, $f_avatar;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status_teacher'], 'required'],
            [['user_id', 'district_id', 'level_id', 'type_id', 'organ_id', 'lang_id', 'status_teacher', 'created_at', 'updated_at'], 'integer'],
            [['birthday', 'level_date'], 'safe'],
            [['first_name', 'last_name', 'middle_name', 'phone', 'address', 'experience', 'cv', 'passport', 'diploma', 'inn', 'inps', 'avatar'], 'string', 'max' => 255],
            [['f_inn', 'f_inps', 'f_cv', 'f_passport', 'f_diploma', 'f_avatar'], 'file', 'extensions' => 'jpg, png, doc, docx, pdf'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['district_id' => 'id']],
            [['lang_id'], 'exist', 'skipOnError' => true, 'targetClass' => Languages::className(), 'targetAttribute' => ['lang_id' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Levels::className(), 'targetAttribute' => ['level_id' => 'id']],
            [['organ_id'], 'exist', 'skipOnError' => true, 'targetClass' => Organisations::className(), 'targetAttribute' => ['organ_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubjectTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Foydalanuvchi ID si'),
            'first_name' => Yii::t('app', 'Ismi'),
            'last_name' => Yii::t('app', 'Familiyasi'),
            'middle_name' => Yii::t('app', 'Otasining ismi'),
            'birthday' => Yii::t('app', 'Tug\'ilgan kuni'),
            'phone' => Yii::t('app', 'Telefon raqami'),
            'district_id' => Yii::t('app', 'Tuman'),
            'address' => Yii::t('app', 'Manzil'),
            'level_id' => Yii::t('app', 'Toifa'),
            'level_date' => Yii::t('app', 'Toifani olgan sanasi'),
            'experience' => Yii::t('app', 'Tajriba'),
            'type_id' => Yii::t('app', 'Fan sohasi'),
            'organ_id' => Yii::t('app', 'Tashkilot'),
            'lang_id' => Yii::t('app', 'Tili'),
            'cv' => Yii::t('app', 'Cv'),
            'passport' => Yii::t('app', 'Passport'),
            'diploma' => Yii::t('app', 'Diplom'),
            'inn' => Yii::t('app', 'Inn'),
            'inps' => Yii::t('app', 'Inps'),
            'status_teacher' => Yii::t('app', 'Holati'),
            'created_at' => Yii::t('app', 'Tuzilgan'),
            'updated_at' => Yii::t('app', 'O\'zgartirilgan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['id' => 'district_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLang()
    {
        return $this->hasOne(Languages::className(), ['id' => 'lang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Levels::className(), ['id' => 'level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrgan()
    {
        return $this->hasOne(Organisations::className(), ['id' => 'organ_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(SubjectTypes::className(), ['id' => 'type_id']);
    }
}
