<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "subjects".
 *
 * @property int $id
 * @property string $name_uz
 * @property string $name_ru
 * @property int $type_id
 * @property int $created_at
 * @property int $updated_at
 *
 * @property Questions[] $questions
 * @property SubjectTypes $type
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_uz', 'name_ru'], 'required'],
            [['type_id', 'created_at', 'updated_at'], 'integer'],
            [['name_uz', 'name_ru'], 'string', 'max' => 255],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubjectTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name_uz' => Yii::t('app', 'Nomi (o\'zb)'),
            'name_ru' => Yii::t('app', 'Nomi (rus)'),
            'type_id' => Yii::t('app', 'Turi'),
            'created_at' => Yii::t('app', 'Tuzilgan'),
            'updated_at' => Yii::t('app', 'O\'zgartirilgan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Questions::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(SubjectTypes::className(), ['id' => 'type_id']);
    }
}
